# gnome-shell-extension-move-resize
by orzun

## Install:
```
mkdir -p ~/.local/share/gnome-shell/extensions/move-resize@orzun
cp ./extension.js ./metadata.json ~/.local/share/gnome-shell/extensions/move-resize@orzun
```

## Usage:
```
gdbus call --session --dest org.gnome.Shell --object-path /org/gnome/Shell/Extensions/MoveResize --method org.gnome.Shell.Extensions.MoveResize.Call "'firefox'" 1 0 0 400 800
```
